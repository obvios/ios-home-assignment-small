//
//  SodraAPI.swift
//  ScroogeMcDuck
//
//  Created by Eric Palma on 12/15/23.
//

import Foundation


struct SodraAPI {
    func sendTaxInfoToSodra(_ salary: String) -> Void {
        print("Sending to Sodra... \(salary)")
    }
}
