//
//  Calculator.swift
//  ScroogeMcDuck
//
//  Created by Eric Palma on 12/15/23.
//

import Foundation

/*
 For better testability, could inject rates instead.
 */
struct SalaryCalculator {
    private let basePensionRate =  0.1252
    private let taxRate = 0.2
    // Took an educated guess here
    private let healthInsuranceDeductionRate = 0.0698
    
    func calculateTakeHomePay(salary: Double, additionalContribution: Double) -> Double {
        // calculate deductions
        let pensionRate = basePensionRate + additionalContribution
        let pensionDeduction = salary * pensionRate
        let taxDeduction = salary * taxRate
        let healthInsuranceDeduction = salary * healthInsuranceDeductionRate
        let totalDeduction = taxDeduction + healthInsuranceDeduction + pensionDeduction
        // calculate take home pay after deductions
        let pay = salary - totalDeduction
        return pay.rounded()
    }
}
