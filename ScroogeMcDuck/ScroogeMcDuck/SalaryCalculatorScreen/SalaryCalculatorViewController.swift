import UIKit
import Combine

class SalaryCalculatorViewController: UIViewController {
    typealias Contribution = PensionContribution
    private let viewModel = SalaryCalculatorViewModel()
    private var cancellables = Set<AnyCancellable>()
    
    @IBOutlet weak var salaryInputTextField: UITextField!
    @IBOutlet weak var takeHomeSalaryLabel: UILabel!
    @IBOutlet weak var additionalPensionOption: UISegmentedControl!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        observeViewModel()
    }
    
    private func setupViews() {
        salaryInputTextField.placeholder = "Enter salary on paper"
        salaryInputTextField.keyboardType = .numberPad
        salaryInputTextField.delegate = self
        additionalPensionOption.removeAllSegments()
        additionalPensionOption.insertSegment(withTitle: Contribution.none.textRepresentation,
                                              at: Contribution.none.rawValue,
                                              animated: true)
        additionalPensionOption.insertSegment(withTitle: Contribution.twoPointOnePercent.textRepresentation,
                                              at: Contribution.twoPointOnePercent.rawValue,
                                              animated: true)
        additionalPensionOption.insertSegment(withTitle: Contribution.threePercent.textRepresentation,
                                              at: Contribution.threePercent.rawValue,
                                              animated: true)
        additionalPensionOption.selectedSegmentIndex = 0
        takeHomeSalaryLabel.textAlignment = .center
        takeHomeSalaryLabel.textColor = .systemGreen
        takeHomeSalaryLabel.font = takeHomeSalaryLabel.font.withSize(60)
        takeHomeSalaryLabel.text = ""

        salaryInputTextField.addTarget(self,
                                       action: #selector(salaryInputTextFieldDidChange(textField:)),
                                       for: .editingChanged)
        additionalPensionOption.addTarget(self,
                                          action: #selector(additionalPensionSelected(sender:)),
                                          for: .valueChanged)
    }

    @objc
    func salaryInputTextFieldDidChange(textField: UITextField) {
        updateTakeHomeSalary()
    }
    
    @objc
    func additionalPensionSelected(sender: UISegmentedControl) {
        updateTakeHomeSalary()
    }

    /// Called each time a user interacts with a UI component
    private func updateTakeHomeSalary() {
        // get current values from ui components
        let salary = salaryInputTextField.text ?? "0"
        let currentlySelectedSegmentIndex = additionalPensionOption.selectedSegmentIndex
        guard let additionalContribution = Contribution(rawValue: currentlySelectedSegmentIndex) else {
            return
        }
        // call viewmodel
        viewModel.updateTakeHomePay(salary: salary, additionalContribution: additionalContribution)
    }
    
    private func observeViewModel() {
        viewModel.takeHomeSalaryPublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] salary in
                self?.takeHomeSalaryLabel.text = salary
            }.store(in: &cancellables)
    }
}

extension SalaryCalculatorViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
