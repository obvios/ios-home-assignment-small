//
//  CalculatorViewModel.swift
//  ScroogeMcDuck
//
//  Created by Eric Palma on 12/15/23.
//

import Foundation
import Combine

/// Represents pre defined additional pension rates.
enum PensionContribution: Int {
    case none
    case twoPointOnePercent
    case threePercent

    var textRepresentation: String {
        switch self {
        case .none:
            return "No"
        case .twoPointOnePercent:
            return "2.1%"
        case .threePercent:
            return "3%"
        }
    }

    /// Actual percentage value used for calculations.
    var percentageValue: Double {
        switch self {
        case .none:
            return 0.0
        case .twoPointOnePercent:
            return 0.021
        case .threePercent:
            return 0.03
        }
    }
}

/*
 For better testability could inject salary calculator
 and sodra api instead.
 */
class SalaryCalculatorViewModel {
    private let takeHomeSalarySubject = PassthroughSubject<String, Never>()
    /// Observed by UI to update itself
    var takeHomeSalaryPublisher: AnyPublisher<String, Never> {
        takeHomeSalarySubject.eraseToAnyPublisher()
    }

    func updateTakeHomePay(salary: String, additionalContribution: PensionContribution) {
        // if no valid salary value, default to zero.
        let salaryValue = Double(salary) ?? 0.0
        let contributionPercent = additionalContribution.percentageValue
        let calculator = SalaryCalculator()
        let pay = calculator.calculateTakeHomePay(salary: salaryValue, additionalContribution: contributionPercent)
        let payString = String(pay)
        // call api
        sendTaxInfoToSodra(payString)
        // update UI
        takeHomeSalarySubject.send(payString)
    }
    
    private func sendTaxInfoToSodra(_ pay: String) {
        let api = SodraAPI()
        api.sendTaxInfoToSodra(pay)
    }
}
