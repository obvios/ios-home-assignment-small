//
//  ScroogeMcDuckTests.swift
//  ScroogeMcDuckTests
//
//  Created by Arminas Ruzgas on 2020-10-23.
//

import XCTest
import Combine
@testable import ScroogeMcDuck

class ScroogeMcDuckTests: XCTestCase {
    var cancellables: Set<AnyCancellable>!
    
    override func setUp() {
        super.setUp()
        cancellables = []
    }
    
    override func tearDown() {
        cancellables = nil
        super.tearDown()
    }

    func testCalculatorNoSalary() {
        let calculator = SalaryCalculator()
        let pay = calculator.calculateTakeHomePay(salary: 0.0, additionalContribution: 0.25)
        
        XCTAssertEqual(0.0, pay)
    }

    func testCalculatorWithAdditionalContribution() {
        let calculator = SalaryCalculator()
        let additionalContribution = 0.03
        let pay = calculator.calculateTakeHomePay(salary: 100, additionalContribution: additionalContribution)
        
        XCTAssertEqual(58.0, pay)
    }
    
    func testViewModelUpdateTakeHomePay() {
        let viewModel = SalaryCalculatorViewModel()
        let expectation = XCTestExpectation(description: "Take home pay calculation")
        viewModel.takeHomeSalaryPublisher
            .sink(receiveValue: { payString in
                XCTAssertEqual(payString, "575.0")
                expectation.fulfill()
            })
            .store(in: &cancellables)

        viewModel.updateTakeHomePay(salary: "1000", additionalContribution: .threePercent)

        wait(for: [expectation], timeout: 1.0)
    }
    
    func testViewModelUpdateTakeHomePayWithEmptyString() {
        let viewModel = SalaryCalculatorViewModel()
        let expectation = XCTestExpectation(description: "Take home pay calculation")
        viewModel.takeHomeSalaryPublisher
            .sink(receiveValue: { payString in
                XCTAssertEqual(payString, "0.0")
                expectation.fulfill()
            })
            .store(in: &cancellables)

        viewModel.updateTakeHomePay(salary: "", additionalContribution: .threePercent)

        wait(for: [expectation], timeout: 1.0)
    }
}
